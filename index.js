const argon2 = require('argon2');
const requestJson = require('request-json');
const uuid = require('uuid/v4');
const jwt = require('jsonwebtoken');

exports.handler = function(event,context,callback){
    console.log("Create User")
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    const secret = event.stageVariables.argonSecret;

    if(!event.body || !JSON.parse(event.body).password|| !JSON.parse(event.body).email){
        var response = { 
            "statusCode": 403,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": '{ "msg" : "Faltan datos"}',
            "isBase64Encoded": false
        };
        return callback(null,response)  ;
    }
    let body = JSON.parse(event.body);
    argon2.hash(body.password, {type: argon2.argon2id}).then(hash => {
        console.log('el hash a argon2 es: ' + hash);
        var newUser = {
          "_id" : uuid(),
          "nombre" : body.nombre,
          "apellido1" : body.apellido1,
          "apellido2" : body.apellido2,
          "email" : body.email,
          "fecha_alta" : Date.now(),
          "tipo" : 0,
          "activo" : true,
          "password" : hash
        };
        console.log(newUser)
        var httpClient = requestJson.createClient(db);
            var query="user?q={\"email\": \""+ body.email + "\"}&" + apikey
            console.log(query)
            httpClient.get(query, newUser, function(err,resMLab,body){
                if(err){
                    console.log("error feo")
                } else {
                    if(body.length == 0 ){
                        httpClient.post("user?" + apikey, newUser, function(err,resMLab,body){
                            if(err){
                                var response = {
                                    "statusCode" : 501,
                                    "headers": {"Access-Control-Allow-Origin": "*"},
                                    "body": "{\"msg\" : \"JSON.stringify(err)\" }",
                                }
                            } else {
                                var token = jwt.sign(newUser, secret, { expiresIn : '1h' });
                                var response = { 
                                    "statusCode": 200,
                                    "headers": {
                                        "X-token": token,
                                        "Access-Control-Allow-Origin": "*"
                                    },
                                    "body": '{"id" : "' + newUser._id + '", "name" : "' + newUser.nombre + '","token" : "' + token + '" }',
                                    "isBase64Encoded": false
                                };
                            }
                            callback(null,response);
                        })
                    } else {
                        var response = { 
                            "statusCode": 405,
                            "headers": {"Access-Control-Allow-Origin": "*"},
                            "body": "{\"msg\":\"Ya existe un usuario con ese correo\"}",
                            "isBase64Encoded": false
                        };
                        callback(null,response);
                    }
                }
            })

    }).catch(err => {
        console.log("Algo ha ido mal :( " + err);
        var response = {
            "statusCode" : 500,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": "{\"msg\" : \"JSON.stringify(err)\" }",
        }
        callback(null,response); 
    });
}


